# Recipe App API Proxy

Django docs recommend you use a proxy in front of your application to serve static files. NGINX serves these files more efficiently.

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
